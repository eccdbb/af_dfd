function [r,c] = round_32(dims)
% dims: [#rows, #columns]
% ex. dims = shape(grayscale_img)
% 
% round upwards so resulting dimensions are divisible by 32

N = 32;
rem_rows = rem(dims(1),N);
rem_cols = rem(dims(2),N);
rem_all = [rem_rows, rem_cols];
dims_out = dims + N - rem_all;
r = dims_out(1);
c = dims_out(2);
end