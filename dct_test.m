clc; clear all; close all;

img = imread('H.jpg'); % A to H .jpg

img = double(img)/255;
img = sum(img,3) / 3; % lazy convert to grayscale

img = imresize(img,0.25,'bicubic');

img_dct = dct2(img);
img_dct2 = abs(img_dct);

figure;
set(gcf,'position',[50 50 1500 1900])
subplot(211)
imshow(img)
subplot(212)
imshow(img_dct2)

x = sum(img_dct2(:))

