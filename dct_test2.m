clc; clear all; close all;

files = {
    'P1520343.jpg',
    'P1520344.jpg',
    'P1520345.jpg',
    'P1520346.jpg',
    'P1520347.jpg',
    'P1520348.jpg',
    'P1520349.jpg'};

N = 32; % size of image patch used for dct2
p.N = N;

x_all = zeros(length(files),1);
f_all_files = {};
for ii = 1:length(files)
    fprintf('-- file %.0f ----------------------\n',ii)
    fn = files{ii};
    img = imread(fn);
    img = double(img)/255;
    img = sum(img,3) / 3; % lazy convert to grayscale

    img = imresize(img,0.25,'bicubic');
    
    % crop region of interest (center row, center column, half-width row, half-width column)
    dims = size(img);
    p.center_row = round(dims(1)*0.50);
    p.center_column = round(dims(2)*0.50);
    
    [half_width_row,half_width_column] = round_32(round(dims*0.15));
    p.half_width_row = half_width_row;
    p.half_width_column = half_width_column;
    %img_cropped = crop(img,round(dims(1)*0.50),round(dims(2)*0.50),round(dims(1)*0.15),round(dims(2)*0.15));
    img_cropped = crop(img,p.center_row,p.center_column,p.half_width_row,p.half_width_column);
    
    %----------------------------
    % compute feature x of entire img_cropped
    img_dct = dct2(img_cropped);
    img_dct2 = abs(img_dct);
    
    x = sum(img_dct2(:));
    x_all(ii) = x;
    %----------------------------
    % take img_cropped and split it into NxN patches
    % then compute features for all patches
    % and save as a training data point
    
    % dims of img_cropped: [p.half_width_row*2, p.half_width_column*2] -> [d1,d2]
    % # patches: (d1/N)*(d2/N)
    p.N_patch_rows = p.half_width_row*2/N;
    p.N_patch_columns = p.half_width_column*2/N;
    p.N_patches = p.N_patch_rows * p.N_patch_columns;
    
    %figure
    f_all = zeros(p.N_patch_rows,p.N_patch_columns);
    for i = 1:p.N_patch_rows
        for j = 1:p.N_patch_columns
            img_patch = img_cropped((i-1)*p.N+1:i*p.N , (j-1)*p.N+1:j*p.N);
            f = compute_feature(img_patch);
            
            f_all(i,j) = f;
            f_all_files{ii} = f_all(:);
            disp(f)
            
            % TODO: see paper on how to save data points
            % need:
            % - main object at various known depths
            % - f at two known focus positions
            % 
            % can't do this b/c don't know focus motor position
            % can try: G85: reset to nearest focus; focus sweep 20 images; take 1st and 10th
        end
    end
    
    
end

figure;
plot(x_all,'o-')
xlabel('focus position (no units)')
ylabel('sum of abs DCT')
set(gcf,'position',[50 50 700 500])
set(gca,'XTickLabel',[])

figure;
hold on;
for ii = 1:length(files)
    plot(ii,f_all_files{ii},'+')
end
xlabel('focus position (no units)')
ylabel('sum of abs DCT of each patch')
set(gcf,'position',[750 50 700 500])
set(gca,'XTickLabel',[])

% DONE: crop out the region of interest; currently using entire frame
% DONE: use blocks from the crop to compute DCT feature set

