function x = compute_feature(img)

img_dct = dct2(img);
img_dct2 = abs(img_dct);
x = sum(img_dct2(:));
end