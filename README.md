# Autofocus DFD #
Re-implementation of the paper:
Fast and accurate auto focusing algorithm based on two defocused images using discrete cosine transform
Park, et al
SPIE 2008

https://www.researchgate.net/publication/252969631_Fast_and_accurate_auto_focusing_algorithm_based_on_two_defocused_images_using_discrete_cosine_transform

## Hardware
Panasonic G85 (supports automatic focus sweep)

## Software
MATLAB

# DONE
- crop image
- DCT and feature of entire image
- split crop region further and calculate DCT and feature; then build dataset

# TODO
- train classifier that returns the open loop focus amount
