function img_cropped = crop(img,i_center,j_center,i_half,j_half)
% i,j: row,column
% crop a grayscale image from the center +/- half height/width
% example:
% crop at (400,300)+/-(100,100) => (300:500,200:400)
% 
% if crop results in out of bounds, clamp it inbounds
% while retaining the crop window size


dims = size(img);
if length(dims)==2
    r = dims(1);
    c = dims(2);
elseif length(dims)==3
    r = dims(1);
    c = dims(2);
    ch = dims(3);
else
    disp('warning: image check image size')
end

% calculate inner boundary
inner_top = i_half;
inner_bottom = r - i_half;
inner_left = j_half;
inner_right = c - j_half;

% clamp
if i_center < inner_top
    i_center_new = inner_top;
elseif i_center > inner_bottom
    i_center_new = inner_bottom;
else
    i_center_new = i_center;
end

if j_center < inner_left
    j_center_new = inner_left;
elseif j_center > inner_right
    j_center_new = inner_right;
else
    j_center_new = j_center;
end

if exist('ch','var') % if the variable 'ch' exists
    img_cropped = [];
    for i = 1:ch
        temp = img(1+i_center_new-i_half:i_center_new+i_half , 1+j_center_new-j_half:j_center_new+j_half , i);
        img_cropped = cat(3,img_cropped,temp);
    end
else
    img_cropped = img(1+i_center_new-i_half:i_center_new+i_half , 1+j_center_new-j_half:j_center+j_half);
end

% test:
%{
dims = size(img);
imshow(crop(img(:,:,2),round(dims(1)/2),round(dims(2)/2),round(dims(1)*0.2),round(dims(2)*0.2)))
imshow(crop(img,round(dims(1)*0.95),round(dims(2)*0.15),round(dims(1)*0.25),round(dims(2)*0.25)))
%}